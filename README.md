## CODEOWNER Approval Proposal

This project demonstrates setting up approvals for all files in a project, while excluding a specific path of files that don't need to be approved.

Specifically we're:

> Requiring approval on all changes EXCEPT `.md` files in the `/doc` path

In addition to the CODEOWNER configuration the following approval settings are also enforced:

![img/approval-settings.png](img/approval-settings.png)
